package edu.uoc.daada_pac3_notifications

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import edu.uoc.daada_pac3_notifications.model.BookContent
import edu.uoc.daada_pac3_notifications.model.BookItem
import kotlinx.android.synthetic.main.book_details.view.*

import edu.uoc.daada_pac3_notifications.model.BookDao

class BookDetailFragment : Fragment() {

    private lateinit var book: BookItem
    private lateinit var roomDatabase: BookContent
    private lateinit var bookDAO: BookDao

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {

        roomDatabase = BookContent.getAppDataBase(activity!!.applicationContext)!!
        bookDAO = roomDatabase.bookDataAccesObject()

        val hostView = inflater.inflate(R.layout.book_details, container, false)

        if(arguments?.containsKey("BOOK_ITEM_IDENTIFIER")!!) {
            val bookIdentifier: Int = arguments!!.getInt("BOOK_ITEM_IDENTIFIER")

            book = bookDAO.getBook(bookIdentifier) ?: bookDAO.getAllBooks().first()

            hostView.author.text = book.author
            hostView.publicationDate.text = book.publicationDate
            hostView.description.text = book.description
        }

        return hostView
    }
}
