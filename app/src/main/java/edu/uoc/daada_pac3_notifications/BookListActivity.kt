package edu.uoc.daada_pac3_notifications

import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.widget.TextView
import android.widget.Toast
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.google.firebase.iid.FirebaseInstanceId

import kotlinx.android.synthetic.main.activity_book_list.*
import kotlinx.android.synthetic.main.book_list.*

import edu.uoc.daada_pac3_notifications.model.BookContent
import edu.uoc.daada_pac3_notifications.model.BookDao
import edu.uoc.daada_pac3_notifications.model.BookItem
import kotlinx.android.synthetic.main.book_list_even_element.view.*

class BookListActivity : AppCompatActivity() {

    private var bookList = emptyList<BookItem>()
    private var twoPanelMode: Boolean = false
    private var recyclerAdapter: SimpleItemRecyclerViewAdapter? = null

    // Instances of FirebaseAuth and FirebaseDatabase
    private lateinit var firebaseAuthorization: FirebaseAuth
    private lateinit var firebaseDatabase: FirebaseDatabase
    private lateinit var firebaseMessagingToken: String

    // Instance of Room
    private lateinit var roomDatabase: BookContent
    private lateinit var bookDAO: BookDao

    val emailFirebase = "perebohigas@uoc.edu"
    val passwordFirebase = "123456"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_book_list)

        initializeDataBase()
        initializeFirebase()

        if (firebaseAuthorization.currentUser == null) {
            loginInFirebase()
        }

        twoPanelMode = checkIfFrameForBookDetailIsPresent()
        setActionBar()
        initializeRecyclerView()

        loadBooksFromRoomDatabase()

        catchInitialIntentAndProceed()
    }

// =====================================================================================================================
// =================================================== NOTIFICATIONS ===================================================
// =====================================================================================================================

// ===================================== START OF CODE TO BE COMPLETED - EXERCISE 3 ====================================
    fun catchInitialIntentAndProceed() {
        if (intent != null) {
            // The App was intialized from an Intent
            if (intent.action != null) {
                // The Intent has an associated action
                if (intent.hasExtra(getString(R.string.intent_data_book_identifier))) {
                    // The Intent has a book identifier and this could be parsed to an int
                    val bookIdentifierFromIntent: Int = intent.getIntExtra(getString(R.string.intent_data_book_identifier), -1)
                    when (intent.action) {
                        getString(R.string.action_show_details) -> {
                            Log.i(getString(R.string.app_name),"App started from a notification with the action to show the details of the book with the id [${bookIdentifierFromIntent}]")
                            showBookDetails(bookIdentifierFromIntent)
                        }
                        getString(R.string.action_delete_book_locally) -> {
                            Log.i(getString(R.string.app_name),"App started from a notification with the action to delete the book with the id [${bookIdentifierFromIntent}]")
                            bookList.forEach {
                                if (it.identifier.equals(bookIdentifierFromIntent)){
                                    deleteBookLocally(it)
                                }
                            }
                        }
                    }
                }
            } else {
                Log.i(getString(R.string.app_name),"App started from a notification without any selected action. It would start normally.")
            }
            // Delete all notifications from this App
            val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.cancelAll()
        }
    }
// ====================================== END OF CODE TO BE COMPLETED - EXERCISE 3 =====================================

// =====================================================================================================================
// ============================================ BACKEND DATABASE (FIREBASE) ============================================
// =====================================================================================================================

    fun initializeFirebase() {
        // Initialize instances of FirebaseAuth and FirebaseDatabase
        firebaseAuthorization = FirebaseAuth.getInstance()
        firebaseDatabase = FirebaseDatabase.getInstance()
        generateFirebaseToken()
    }

    fun loginInFirebase() {
        firebaseAuthorization.signInWithEmailAndPassword(emailFirebase, passwordFirebase)
            .addOnCompleteListener(this) { task ->
                val toast: Toast
                if (task.isSuccessful) {
                    // Sign in succeeded
                    val user = firebaseAuthorization.currentUser
                    downloadBooksFromFirebase()
                    Log.i(getString(R.string.app_name), "Authentication with Firebase success with email [${user!!.email}]")

                    toast = Toast.makeText(baseContext, getString(R.string.authentication_message_success), Toast.LENGTH_SHORT)
                    toast.setGravity(Gravity.TOP, 0, 0)
                } else {
                    // Sign in failed
                    Log.w(getString(R.string.app_name), "Authentication with Firebase failure", task.exception)
                    toast = Toast.makeText(baseContext, getString(R.string.authentication_message_fail), Toast.LENGTH_SHORT)
                }
                // Display a toast message to the user with the result of the authentication
                toast.show()
            }
    }

// ===================================== START OF CODE TO BE COMPLETED - EXERCISE 1 ====================================
    fun generateFirebaseToken() {
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w(getString(R.string.app_name), "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }

                // Get new Instance ID token
                firebaseMessagingToken = task.result?.token!!

                // Log and toast
                Log.i(getString(R.string.app_name), "Get the following token for the firebase messaging: ${firebaseMessagingToken}")
            })
    }
// ====================================== END OF CODE TO BE COMPLETED - EXERCISE 1 =====================================

    fun downloadBooksFromFirebase() {
        if (firebaseAuthorization.currentUser != null) {
            var reference: DatabaseReference = firebaseDatabase.getReference("books")

            reference.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    loadBooksFromDataSnapshot(dataSnapshot)
                    Log.i(
                        getString(R.string.app_name),
                        "Books from Firebase database loaded successfully, and new ones added in local database"
                    )
                }

                override fun onCancelled(error: DatabaseError) {
                    Log.w(
                        getString(R.string.app_name),
                        "Error loading books list from Firebase database, showing only books from local database"
                    )
                }
            })
        }
    }

    private fun loadBooksFromDataSnapshot(dataSnapshot: DataSnapshot) {
        // Function to parse BookItem objects from a Firebase database DataSnapshot

        //val genericTypeIndicator: GenericTypeIndicator<ArrayList<BookItem>> = object: GenericTypeIndicator<ArrayList<BookItem>>() {}

        for (childDataSnapshot in dataSnapshot.getChildren()) {
            val identifier: Int = (childDataSnapshot.getKey() as String).toInt()
            val title: String = childDataSnapshot.child("title").getValue() as String
            val author: String = childDataSnapshot.child("author").getValue() as String
            val publicationDate: String = childDataSnapshot.child("publication_date").getValue() as String
            val description: String = childDataSnapshot.child("description").getValue() as String
            val imageURL: String = childDataSnapshot.child("url_image").getValue() as String

            val book = BookItem(identifier, title, author, publicationDate, description, imageURL)
            loadNewBookInRoomDatabase(book)
        }
    }

    private fun refreshBookListFromFirebase() {
        var reference: DatabaseReference = firebaseDatabase.getReference("books")
        reference.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                loadBooksFromDataSnapshot(dataSnapshot)
                loadBooksFromRoomDatabase()
            }

            override fun onCancelled(databaseError: DatabaseError) {
            }
        })
    }

// =====================================================================================================================
// =============================================== LOCAL DATABASE (ROOM) ===============================================
// =====================================================================================================================

    fun initializeDataBase() {
        // Initialize Room Database
        roomDatabase = BookContent.getAppDataBase(this)!!
        bookDAO = roomDatabase.bookDataAccesObject()
    }

    fun loadBooksFromRoomDatabase() {
        bookList = bookDAO.getAllBooks()
        cleanDuplicatesInRoomDatabase()
        bookList.sortedBy { it.identifier }
        recyclerAdapter!!.setItems(bookList)
    }

    fun loadNewBookInRoomDatabase(newBook: BookItem) {
        if (bookDAO.getDuplicates(newBook.title, newBook.author, newBook.publicationDate).isNullOrEmpty()) {
            bookDAO.addBook(newBook)
            Log.i(getString(R.string.app_name), "Added Book ${newBook} into Room Database")
        }
    }

    fun deleteBookLocally(book: BookItem) {
        bookDAO.deleteBook(book)
        loadBooksFromRoomDatabase()
    }

    fun cleanDuplicatesInRoomDatabase() {
        bookList.forEach {
            if (bookDAO.getDuplicates(it.title, it.author, it.publicationDate)!!.count() > 1) {
                bookDAO.deleteBook(it)
            }
        }
    }

// =====================================================================================================================
// =================================================== USER INTERFACE ==================================================
// =====================================================================================================================

    fun showBookDetails(bookIdentifier: Int) {
        if(twoPanelMode) {
            // Start the fragment corresponding to a tablet, sending as argument the selected position
            val newFragment = BookDetailFragment().apply {
                arguments = Bundle()
                arguments!!.putInt("BOOK_ITEM_IDENTIFIER", bookIdentifier)
            }
            Log.i(getString(R.string.app_name), "Add fragment to this activity to show the details of the book with the identifier ${bookIdentifier}")
            supportFragmentManager.beginTransaction()
                .replace(R.id.frame_for_book_details, newFragment)
                .commit()
        } else {
            // Start the activity corresponding to a smartphone, sending as argument the selected position
            Log.i(getString(R.string.app_name), "Start BookDetailsActivity to show the details of the book with the identifier ${bookIdentifier}")
            val intent = Intent(this, BookDetailActivity::class.java)
            intent.putExtra("BOOK_ITEM_IDENTIFIER", bookIdentifier)
            startActivity(intent)
        }
    }

    fun initializeRecyclerView() {
        var linearLayoutManager = LinearLayoutManager(this)
        val recyclerView: RecyclerView = findViewById(R.id.list_of_books)
        recyclerView.layoutManager = linearLayoutManager
        recyclerAdapter = SimpleItemRecyclerViewAdapter(bookList, this)
        recyclerView.adapter = recyclerAdapter
    }

    fun checkIfFrameForBookDetailIsPresent(): Boolean {
        if (frame_for_book_details == null) {
            Log.i(getString(R.string.app_name),"Book details is NOT been shown [small screen]")
            return false
        } else {
            Log.i(getString(R.string.app_name),"Book details is been shown [big screen]")
            return true
        }
    }

    class SimpleItemRecyclerViewAdapter(private var listBookItems: List<BookItem>, private val context: Context) : RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder>() {
        private val EVEN = 0
        private val ODD = 1

        override fun getItemCount(): Int {
            return listBookItems.size
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.mItem = listBookItems[position]
            holder.mTitleView.text = listBookItems[position].title
            holder.mAuthorView.text = listBookItems[position].author

            // Saves the current position of the view's holder
            var bookItemPosition = position

            holder.itemView.setOnClickListener { _ ->
                var currentPosition: Int

                // Get the position of the book which was clicked in the view
                currentPosition = bookItemPosition

                val selectedBook: BookItem = listBookItems[currentPosition]

                Log.i(context.getString(R.string.app_name), "The book ${selectedBook} in the position ${currentPosition} has been clicked")

                if (context is BookListActivity) {context.showBookDetails(selectedBook.identifier)}
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            var view: View? = null

            // Load a diferent layout for even and odd elements
            if (viewType == EVEN) {
                view = LayoutInflater.from(parent.context).inflate(R.layout.book_list_even_element, parent, false)
            } else if (viewType == ODD) {
                view = LayoutInflater.from(parent.context).inflate(R.layout.book_list_odd_element, parent, false)
            }
            return ViewHolder(view!!)
        }

        override fun getItemViewType(position: Int): Int {
            // Return type even or odd according to its position
            var type: Int
            if (position % 2 == 0) {
                type = EVEN
            } else {
                type = ODD
            }
            return type
        }

        class ViewHolder(elementView: View) : RecyclerView.ViewHolder(elementView) {
            lateinit var mItem: BookItem
            val mTitleView: TextView = elementView.titleView
            val mAuthorView: TextView = elementView.authorView
        }

        fun setItems(bookItems: List<BookItem>) {
            listBookItems = bookItems
            notifyDataSetChanged()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_book_list, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.by_title) {
            recyclerAdapter?.setItems(roomDatabase.bookDataAccesObject().getAllBooks().sortedWith(compareBy({ it.title })))
            return true
        } else if (item.itemId == R.id.by_author) {
            recyclerAdapter?.setItems(roomDatabase.bookDataAccesObject().getAllBooks().sortedWith(compareBy({ it.author })))
            return true
        } else if (item.itemId == R.id.download_books_from_firebase) {
            refreshBookListFromFirebase()
            return true
        } else {
            return super.onOptionsItemSelected(item)
        }
    }

    fun setActionBar() {
        setSupportActionBar(toolbar)
        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }
    }
}
