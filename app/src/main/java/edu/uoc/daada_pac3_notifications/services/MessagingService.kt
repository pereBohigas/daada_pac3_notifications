// ===================================== START OF CODE TO BE COMPLETED - EXERCISE 1 ====================================
package edu.uoc.daada_pac3_notifications.services

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.media.AudioAttributes
import android.media.RingtoneManager
import android.os.Build
import android.support.v4.app.NotificationCompat
import android.support.v4.content.ContextCompat
import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import edu.uoc.daada_pac3_notifications.BookListActivity
import edu.uoc.daada_pac3_notifications.R

class MessagingService: FirebaseMessagingService() {

    lateinit var notificationManager: NotificationManager
    lateinit var notificationBuilder: NotificationCompat.Builder
    lateinit var notificationChannel: NotificationChannel

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        // Show a notification after receiving a message from Firebase
        val stringBuilder = StringBuilder("Received a")
        var middleMessage = "notification with title [${remoteMessage.notification?.title}] and message [${remoteMessage.notification?.body}]"
        if (remoteMessage.data.isEmpty())  stringBuilder.append(" generic ").append(middleMessage) else stringBuilder.append(" custom ").append(middleMessage).append(". Which also includes the data [${remoteMessage.data!!}]")
        Log.i(getString(R.string.app_name), stringBuilder.toString())

        sendNotification(remoteMessage.notification?.title!!, remoteMessage.notification?.body!!, remoteMessage.data!!)
    }

    override fun onNewToken(token: String?) {
        Log.d(getString(R.string.app_name), "Refreshed token: $token")
        // TODO: Send the refreshed Instance ID token to the app server.
    }

    fun sendNotification(title: String, body: String, data: Map<String, String>? = null) {
        buildNotification(title, body)
        if (data != null) addActionsToNotification(data)
        notificationManager.notify(System.currentTimeMillis().toInt(), notificationBuilder.build())
    }
// ====================================== END OF CODE TO BE COMPLETED - EXERCISE 1 =====================================

// ===================================== START OF CODE TO BE COMPLETED - EXERCISE 2 ====================================
    fun buildNotification(title: String, body: String) {
        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        notificationBuilder = NotificationCompat.Builder(this, getString(R.string.notification_channel_1_id))
                .setContentTitle(title)
                .setContentText(body)
                .setAutoCancel(true)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentIntent(PendingIntent.getActivity(this, 0, buildBookListActivityIntent(), PendingIntent.FLAG_ONE_SHOT))
                .setPriority(NotificationCompat.PRIORITY_HIGH)

        setSmallIconAccordingToAndroidVersion()
        buildNotificationsChannelIfNeeded()
        setSoundVibrationAndLedIfPossible()
    }

    fun addActionsToNotification(data: Map<String, String>) {
        for ((key, value) in data) {
            when(key) {
                "book_id" -> {
                    val intentToShowDetails = PendingIntent.getActivity(this, System.currentTimeMillis().toInt(),
                        buildBookListActivityIntent(getString(R.string.action_show_details), value.toIntOrNull()),0)
                    notificationBuilder.addAction(NotificationCompat.Action(0, "Show details", intentToShowDetails))

                    val intentToDeleteBook = PendingIntent.getActivity(this, System.currentTimeMillis().toInt(),
                        buildBookListActivityIntent(getString(R.string.action_delete_book_locally), value.toIntOrNull()),0)
                    notificationBuilder.addAction(NotificationCompat.Action(0, "Delete locally", intentToDeleteBook))
                }
            }
        }
    }

    fun buildBookListActivityIntent(intentAction: String? = null, bookIdentifier: Int? = null): Intent {
        var newIntent = Intent(this, BookListActivity::class.java)
        if (intentAction != null && bookIdentifier != null) {
            newIntent.action = intentAction
            newIntent.putExtra(getString(R.string.intent_data_book_identifier), bookIdentifier)
        }
        newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        return newIntent
    }

    fun setSmallIconAccordingToAndroidVersion() {
        // Check if the app is running on an Android 5.0 (Lollipop) or higher
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setSmallIcon(R.drawable.book_symbol)
            notificationBuilder.setColor(ContextCompat.getColor(this, R.color.uoc_launcher_background))
        } else {
            notificationBuilder.setSmallIcon(R.mipmap.book_symbol)
        }
    }

    fun buildNotificationsChannelIfNeeded() {
        // Check if the app is running on an Android 8.0 (Oreo) or higher
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val numberOfNotifications: Int = notificationManager.activeNotifications.size
            notificationChannel = NotificationChannel(
                if (numberOfNotifications % 2 == 0) getString(R.string.notification_channel_1_id) else getString(R.string.notification_channel_2_id),
                if (numberOfNotifications % 2 == 0) getString(R.string.notification_channel_1_name) else getString(R.string.notification_channel_2_name),
                NotificationManager.IMPORTANCE_HIGH
            )
            if (numberOfNotifications % 2 == 0) notificationBuilder.setChannelId(getString(R.string.notification_channel_1_id)) else notificationBuilder.setChannelId(getString(R.string.notification_channel_2_id))
            notificationManager.createNotificationChannel(notificationChannel)
        }
    }

    fun setSoundVibrationAndLedIfPossible() {
        val primarySound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val secondarySound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val numberOfNotifications: Int = notificationManager.activeNotifications.size
            val audioAttributes = AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                .build()
            notificationChannel.setSound(if (numberOfNotifications % 2 == 0) primarySound else secondarySound, audioAttributes)
            notificationBuilder.setLights(if (numberOfNotifications % 2 == 0) Color.BLUE else Color.RED, 300, 1000)

        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val numberOfNotifications: Int = notificationManager.activeNotifications.size
            notificationBuilder.setSound(if (numberOfNotifications % 2 == 0) primarySound else secondarySound)
        }
        notificationBuilder.setVibrate(longArrayOf(100, 200, 300, 400, 500, 1000))
    }
// ====================================== END OF CODE TO BE COMPLETED - EXERCISE 2 =====================================
}
